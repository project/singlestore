<?php

namespace Drush\Sql;

/**
 * SingleStore plugin for drush.
 */
class SqlSingleStore extends SqlMysql {

  /**
   * {@inheritdoc}
   */
  public function command() {
    return 'singlestore';
  }

  /**
   * {@inheritdoc}
   */
  public function creds($hide_password = TRUE) {
    return parent::creds(FALSE);
  }

}

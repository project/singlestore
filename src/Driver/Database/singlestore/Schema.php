<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Driver\mysql\Schema as MysqlSchema;
use Drupal\Core\Database\SchemaException;
use Drupal\Core\Database\SchemaObjectDoesNotExistException;
use Drupal\Core\Database\SchemaObjectExistsException;

/**
 * SingleStore implementation of \Drupal\Core\Database\Schema.
 */
class Schema extends MysqlSchema {

  const TABLE_TYPE_REFERENCE = 'reference';
  const SHARD_FIELD = 'shard_id';

  /**
   * Generate SQL to create a new table from a Drupal schema definition.
   *
   * @param string $name
   *   The name of the table to create.
   * @param array $table
   *   A Schema API table definition array.
   *
   * @return array
   *   An array of SQL statements to create the table.
   */
  protected function createTableSql($name, $table) {
    $table = $this->tableSchemaAlter($name, $table);

    // As of version 7.3 of SingleStore, Columnstore is the default table storage format.
    // Define rowstore table type by default.
    $table_type = 'ROWSTORE ';
    if (!empty($table['type']) && $table['type'] == static::TABLE_TYPE_REFERENCE) {
      $table_type = 'REFERENCE ';
    }

    $sql = "CREATE " . $table_type . "TABLE {" . $name . "} (\n";

    if (empty($table['shard key']) && !$this->isReference($table)) {
      // If shard key isn't set for the table set it here.
      $table['fields'][static::SHARD_FIELD] = [
        'description' => 'Shard id',
        'type' => 'varchar_ascii',
        'length' => 12,
        'not null' => TRUE,
        'default' => '',
      ];
      $table['shard key'] = ['shard_id'];
    }

    $this->ensureReferenceTableEmptyShardKey($table);

    // Add the SQL statement for each field.
    foreach ($table['fields'] as $field_name => $field) {
      $sql .= $this->createFieldSql($field_name, $this->processField($field)) . ", \n";
    }

    // Process keys & indexes.
    if (!empty($table['primary key']) && is_array($table['primary key'])) {
      $this->ensureNotNullPrimaryKey($table['primary key'], $table['fields']);
    }
    $keys = $this->createKeysSql($table);
    if (count($keys)) {
      $sql .= implode(", \n", $keys) . ", \n";
    }

    // Remove the last comma and space.
    $sql = substr($sql, 0, -3) . "\n) ";

    // Add table comment.
    if (!empty($table['description'])) {
      $sql .= ' COMMENT ' . $this->prepareComment($table['description'], static::COMMENT_MAX_TABLE);
    }

    return [$sql];
  }

  /**
   * {@inheritdoc}
   */
  protected function createKeysSql($spec) {
    $keys = [];

    if (!empty($spec['primary key'])) {
      $primary_keys = $this->generateKeys($spec['primary key'], $spec);
      $keys[] = 'PRIMARY KEY (' . $this->createKeySql($primary_keys) . ')';
    }
    if (!empty($spec['unique keys'])) {
      foreach ($spec['unique keys'] as $key => $fields) {
        $fields = $this->generateKeys($fields, $spec);
        $keys[] = 'UNIQUE KEY `' . $key . '` (' . $this->createKeySql($fields) . ')';
      }
    }
    if (!empty($spec['indexes'])) {
      $indexes = $this->getNormalizedIndexes($spec);
      foreach ($indexes as $index => $fields) {
        $keys[] = 'INDEX `' . $index . '` (' . $this->createKeySql($fields) . ')';
      }
    }
    if (!empty($spec['shard key'])) {
      $keys[] = 'SHARD KEY (' . $this->createKeySql($spec['shard key']) . ')';
    }

    return $keys;
  }

  /**
   * Generates correct keys.
   *
   * The primary key or unique key must contain all columns specified in the
   * shard key.
   */
  protected function generateKeys($keys, $spec) {
    if (!empty($spec['shard key'])) {
      $keys = array_merge($keys, $spec['shard key']);
      $keys = array_unique($keys);
    }

    return $keys;
  }

  /**
   * {@inheritdoc}
   */
  public function indexExists($table, $name) {
    // SingleStore supports WHERE in SHOW INDEX
    // https://docs.memsql.com/v7.0/reference/sql-reference/show-commands/show-indexes
    // but it doesn't work to get specific index. So we get all indexes first.
    $row = $this->connection->query('SHOW INDEX FROM {' . $table . '}')->fetchAllAssoc('Key_name');
    return isset($row[$name]);
  }

  /**
   * Alters default table's schema.
   *
   * @param string $name
   *   The name of the table to create.
   * @param array $table
   *   A Schema API table definition array.
   *
   * @return array
   *   Updated table schema.
   */
  protected function tableSchemaAlter($name, array $table) {
    if (!isset($table['type']) && in_array($name, static::referenceTables())) {
      $table['type'] = static::TABLE_TYPE_REFERENCE;
    }
    return $table;
  }

  /**
   * Checks whether table is reference or not.
   */
  protected function isReference($table) {
    if (isset($table['type']) && $table['type'] == static::TABLE_TYPE_REFERENCE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Provides list of tables that have to be created as "reference tables".
   *
   * See explanation of reference tables
   * https://docs.singlestore.com/db/v7.5/en/create-your-database/physical-database-schema-design/concepts-of-physical-database-schema-design/other-schema-concepts.html.
   *
   * @return string[]
   *   Array with tables from Drupal core.
   */
  public static function referenceTables() {
    // @todo: what tables have to be "reference" else except "sequences"?
    return [
      'key_value',
      'key_value_expire',
      'sequences',
      'sessions',
      'batch',
      'config',
      'router',
      'semaphore',
      'queue',
    ];
  }

  /**
   * Ensures that reference table doesn't contain shard keys.
   *
   * @param string $table
   *   A Schema API table definition array.
   *
   * @throws \Drupal\Core\Database\SchemaException
   *   Thrown if table is defined as reference and shard key exists.
   */
  protected function ensureReferenceTableEmptyShardKey($table) {
    if ($this->isReference($table) && isset($table['shard key'])) {
      throw new SchemaException("Sharded reference tables is not supported by SingleStore.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function changeField($table, $field, $field_new, $spec, $keys_new = []) {
    if (!$this->fieldExists($table, $field)) {
      throw new SchemaObjectDoesNotExistException("Cannot change the definition of field $table.$field: field doesn't exist.");
    }
    if (($field != $field_new) && $this->fieldExists($table, $field_new)) {
      throw new SchemaObjectExistsException("Cannot rename field $table.$field to $field_new: target field already exists.");
    }
    if (isset($keys_new['primary key']) && in_array($field_new, $keys_new['primary key'], TRUE)) {
      $this->ensureNotNullPrimaryKey($keys_new['primary key'], [$field_new => $spec]);
    }

    if ($field != $field_new) {
      // Rename of the field.
      $sql_rename = 'ALTER TABLE {' . $table . '} CHANGE `' . $field . '` `' . $field_new . '`)';
      $this->connection->query($sql_rename);
    }

    // Change field.
    $sql = 'ALTER TABLE {' . $table . '} MODIFY `' . $field_new . '` ' . $this->createUpdateFieldSql($field_new, $this->processField($spec));

    if ($keys_sql = $this->createKeysSql($keys_new)) {
      $sql .= ', ADD ' . implode(', ADD ', $keys_sql);
    }
    $this->connection->query($sql);
  }

  /**
   * Create an SQL string for a field to be used in table alteration.
   *
   * @param string $name
   *   Name of the field.
   * @param array $spec
   *   The field specification, as per the schema data structure format.
   */
  protected function createUpdateFieldSql($name, array $spec) {
    $sql = $spec['mysql_type'];

    if (in_array($spec['mysql_type'], $this->mysqlStringTypes)) {
      if (isset($spec['length'])) {
        $sql .= '(' . $spec['length'] . ')';
      }
      if (isset($spec['type']) && $spec['type'] == 'varchar_ascii') {
        $sql .= ' CHARACTER SET ascii';
      }
      if (!empty($spec['binary'])) {
        $sql .= ' BINARY';
      }
      // Note we check for the "type" key here. "mysql_type" is VARCHAR:
      elseif (isset($spec['type']) && $spec['type'] == 'varchar_ascii') {
        $sql .= ' COLLATE ascii_general_ci';
      }
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $sql .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    if (!empty($spec['unsigned'])) {
      $sql .= ' unsigned';
    }

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $sql .= ' NOT NULL';
      }
      else {
        $sql .= ' NULL';
      }
    }

    if (!empty($spec['auto_increment'])) {
      $sql .= ' auto_increment';
    }

    // $spec['default'] can be NULL, so we explicitly check for the key here.
    if (array_key_exists('default', $spec)) {
      $sql .= ' DEFAULT ' . $this->escapeDefaultValue($spec['default']);
    }

    if (empty($spec['not null']) && !isset($spec['default'])) {
      $sql .= ' DEFAULT NULL';
    }

    // Add column comment.
    if (!empty($spec['description'])) {
      $sql .= ' COMMENT ' . $this->prepareComment($spec['description'], self::COMMENT_MAX_COLUMN);
    }

    return $sql;
  }

}

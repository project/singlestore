<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Transaction as DatabaseTransaction;

/**
 * SingleStore implementation of \Drupal\Core\Database\Transaction.
 */
class Transaction extends DatabaseTransaction {}

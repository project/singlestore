<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Query\Select as QuerySelect;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Select.
 */
class Select extends QuerySelect {}

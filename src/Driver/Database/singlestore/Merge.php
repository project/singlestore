<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Query\Merge as QueryMerge;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Merge.
 */
class Merge extends QueryMerge {}

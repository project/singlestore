<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Driver\mysql\Connection as MysqlConnection;

/**
 * SingleStore implementation of \Drupal\Core\Database\Connection.
 */
class Connection extends MysqlConnection {

  /**
   * Provides a map of condition operators to condition operator options.
   *
   * @var array
   */
  protected static $singlestoreConditionOperatorMap = [
    'LIKE' => ['postfix' => ''],
    'NOT LIKE' => ['postfix' => ''],
  ];

  /**
   * {@inheritdoc}
   */
  public function mapConditionOperator($operator) {
    return isset(static::$singlestoreConditionOperatorMap[$operator]) ? static::$singlestoreConditionOperatorMap[$operator] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function driver() {
    return 'singlestore';
  }

  /**
   * {@inheritdoc}
   */
  public function databaseType() {
    return 'singlestore';
  }

  /**
   * {@inheritdoc}
   */
  public function queryTemporary($query, array $args = [], array $options = []) {
    $tablename = $this->generateTemporaryTableName();
    $this->query('CREATE TEMPORARY TABLE {' . $tablename . '} ' . $query, $args, $options);
    return $tablename;
  }

}

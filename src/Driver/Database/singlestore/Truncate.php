<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Query\Truncate as QueryTruncate;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Truncate.
 */
class Truncate extends QueryTruncate {}

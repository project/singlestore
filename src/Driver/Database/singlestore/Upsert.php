<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Driver\mysql\Upsert as MysqlUpsert;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Upsert.
 */
class Upsert extends MysqlUpsert {}

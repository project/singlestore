<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Query\Delete as QueryDelete;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Delete.
 */
class Delete extends QueryDelete {}

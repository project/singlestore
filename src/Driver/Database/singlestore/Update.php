<?php

namespace Drupal\singlestore\Driver\Database\singlestore;

use Drupal\Core\Database\Query\Update as QueryUpdate;

/**
 * SingleStore implementation of \Drupal\Core\Database\Query\Update.
 */
class Update extends QueryUpdate {}
